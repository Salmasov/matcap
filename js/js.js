$(document).ready(function () {
      var formBlock = $('form').eq(0);
      $('form').not('[data-use-script=false]').on('submit', function (event) {
        event.preventDefault();
        formBlock = $(this);
        var data = $(this).serialize();
        var whereSend = $(this).data("pathSend");
        $.ajax({
          url: "/leek.php",
          type: 'POST',
          dataType: 'html',
          data: data,
          success: function (data) {
            var responce = JSON.parse(data);
            if (responce.status == true) {
              $(formBlock).html('<span class="res">Спасибо за заявку! Наш менеджер скоро свяжется с вами.</span>')
            } else {
  
            }
            return false;
          },
          error: function () {
            alert('supererror');
          }
        });
        return false;
      });
    });